<?php
error_reporting(E_ERROR | E_PARSE);
header('Content-Type: application/json');
/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/*GET WDF Games from RSS Feed ------------------------------------------------*/


function getWDFGames()
{
    $feed_url = "http://webdel.futbol/feed/";
    $wdf_img_url = "http://webdel.futbol/wp-content/uploads/wdf-200-new.jpg";
    $content = file_get_contents($feed_url);
    $x = new SimpleXmlElement($content);

    $array = array();

    foreach ($x->channel->item as $entry) {
        $array_element = array();
        $array_element['channelId'] = "null";
        $array_element['title'] = "WDF - " . $entry->title;
        $array_element['categoryId'] = "80";
        $array_element['popularityIndex'] = "0";
        $array_element['logoUrl'] = $wdf_img_url;
        $array_element['bitRate'] = 700000;
        $array_element['link'] = $entry->link;

        $array[] = $array_element;

        /* echo "<li><a href='$entry->link' title='$entry->title'>" . $entry->title . "</a></li>";
          echo "Entry img: " . $x->img; */
    }

    return $array;
}

function getVeetleWidgetLink($link)
{
    $link_content = file_get_contents($link);
    $doc = new DOMDocument();
    $doc->loadHTML($link_content);

    $selector = new DOMXPath($doc);

    foreach ($selector->query('//iframe') as $iframe) {
        $src = $iframe->getAttribute('src');
        
        if (strpos($src,'veetle.com') !== false) {
            return $src;
        }
    }
    
    return NULL;
}


function getJSFromVeetleWidget($veetle_link)
{
    $link_content = file_get_contents($veetle_link);
    $doc = new DOMDocument();
    $doc->loadHTML($link_content);

    $selector = new DOMXPath($doc);

    foreach ($selector->query('//script') as $iframe) {
        $src = $iframe->getAttribute('src');
        
        if (strpos($src,'_minified.js') !== false) {
            return "http://veetle.com".$src;
        }
    }
    
    return NULL;
}

function getChannelIDFromJS($veetle_js_link)
{
    $link_content = file_get_contents($veetle_js_link);
    
    $array = explode("STREAM_CHANNEL_ID_2_AFV_CHANNEL_ID={", $link_content);
    
    if(count($array) >= 2)
    {
        $temp = $array[1];
        $array2 = explode("}", $temp);
        
        if(count($array2) > 0)
        {
            $temp2 = "{".$array2[0]."}";
            //echo "<br/>Array: ".$temp2;
            //echo "<br/><br/>";
            $final_array = json_decode($temp2);
            //$keys = array_keys($final_array);
            
            //return $keys[1];
            //Get the first key - the channel ID
            return key($final_array);
        }
        else
        {
            return NULL;
        }
        
        
    }
    return NULL; 
}

function getTestArray()
{
    $array = array();
    
    $array_element = array();
    $array_element['channelId'] = "null";
    $array_element['title'] = "WDF - ";
    $array_element['categoryId'] = "80";
    $array_element['popularityIndex'] = "0";
    $array_element['logoUrl'] = "http://webdel.futbol/wp-content/uploads/wdf-200-new.jpg";
    $array_element['bitRate'] = 700000;
    $array_element['link'] = "http://pluginwdf.site90.net/Plugin-WDF/TestVeetle.html";
    
    $array[] = $array_element;
    
    return $array;
}


$array_wdf = getWDFGames();
//$array_wdf = getTestArray();

if(count($array_wdf) > 0)
{
    for ($index = 0; $index < count($array_wdf); $index++) {
    
    $game = $array_wdf[$index];
    
    $link_web = $game["link"];
    
    $veetle_link = getVeetleWidgetLink($link_web);
    
    if($veetle_link)
    {
        $veetle_js_link = getJSFromVeetleWidget($veetle_link);
        
        if($veetle_js_link)
        {
            $channel_id = getChannelIDFromJS($veetle_js_link);
            
            if($channel_id)
            {
                $array_wdf[$index]["channelId"] = $channel_id;
                //echo '<br/>Game: '.$game["channelId"];
            }
        }
    }
}
}

echo json_encode($array_wdf);

/*$veetle_link = getVeetleWidgetLink($link_test);
$veetle_js_link = getJSFromVeetleWidget($veetle_link);
$channel_id = getChannelIDFromJS($veetle_js_link);
echo '<br>Veetle link: '.$veetle_link;
echo '<br>Veetle js link: '.$veetle_js_link;
echo '<br>Veetle channel id: '.$channel_id;*/



/*$elements = $doc->getElementsByTagName('iframe');
foreach ($elements as $element) {
    echo "<br/>Lista ql: ".  json_encode($element);
}*/